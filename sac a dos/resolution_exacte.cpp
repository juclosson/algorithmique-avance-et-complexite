#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <ctime>
#include <chrono>
#include <random>

using namespace std;

int NB_OBJETS=7;
int CAPA=10;


/*************************************************/
/****** On genère des vecteur aléatoirement ******/
std::vector<int> genererVecteurAleatoire(int taille, int valeurMin=0, int valeurMax=15) 
{
    // Initialiser le générateur de nombres aléatoires avec une graine basée sur l'horloge actuelle
    std::random_device rd;
    std::mt19937 generator(rd());

    // Définir la distribution pour les valeurs aléatoires entre valeurMin et valeurMax
    std::uniform_int_distribution<int> distribution(valeurMin, valeurMax);

    // Générer le vecteur avec des valeurs aléatoires
    std::vector<int> vecteur(taille);
    for (int& valeur : vecteur) 
    {
        valeur = distribution(generator);
    }
    return vecteur;
}



// une structure est crée pour chaque objet (cela facilitera le tri plus tard)
struct Objet {
    int poids;
    int valeur;
    int indice;

    Objet(int p, int v, int i) : poids(p), valeur(v), indice(i) {}
};

// on introduit un moyen de comparer 2 objets: selon leur efficacité
bool comparateur(const Objet& a, const Objet& b) {
    return (static_cast<double>(a.valeur) / a.poids) > (static_cast<double>(b.valeur) / b.poids);
}
//objet résultats, utile pour ne renvoyer qu'un objet avec les infos voulues
struct Resultat {
    std::vector<int> objets_pris;
    int poids_utilise;
    int valeur_totale;
};





//parcours exacte; on parcours tout l'arbre des solution et on ne garde que la meilleur 
Resultat parcours_exacte(std::vector<Objet>& objets, int capacite, std::vector<std::vector<int>>& memo, int index)
{
    if (index < 0) {
        return Resultat();  // Condition d'arrêt : plus d'objets à considérer
    }

    if (memo[index][capacite] != -1) {
        // La valeur a déjà été calculée pour ces paramètres
        return Resultat();
    }

    Resultat exclure = parcours_exacte(objets, capacite, memo, index - 1);

    if (objets[index].poids <= capacite) {
        Resultat inclure = parcours_exacte(objets, capacite - objets[index].poids, memo, index - 1);
        inclure.poids_utilise += objets[index].poids;
        inclure.valeur_totale += objets[index].valeur;
        inclure.objets_pris.push_back(objets[index].indice);

        if (inclure.valeur_totale > exclure.valeur_totale) {
            memo[index][capacite] = 1;
            return inclure;
        }
    }

    memo[index][capacite] = 1;
    return exclure;
}

// fonction qui permet de créer un liste d"Objet" aprtir des poids et valeurs, utile dans le cas de l'algo récurent
std::vector<Objet> creerListeObjets(const std::vector<int>& poids, const std::vector<int>& valeurs) {
    std::vector<Objet> objets;
    
    // Assurez-vous que les deux vecteurs ont la même taille
    if (poids.size() != valeurs.size()) {
        std::cerr << "Erreur : Les vecteurs poids et valeurs n'ont pas la même taille." << std::endl;
        // Gestion de l'erreur, retour ou traitement approprié
        // Dans cet exemple, je retourne simplement une liste vide
        return objets;
    }

    int n = poids.size();

    for (int i = 0; i < n; ++i) {
        objets.push_back(Objet(poids[i], valeurs[i], i));
    }

    return objets;
}


//fonction d'affichage des variables
void afficher(int capa, vector<int> pds , vector<int>& val) {
    
    cout<<"capacite disponible: "<<capa<<"\n";

    cout<<"Les objets disponibles ";
    cout<<"( [val,poids]) :\n";

    for(int i=0;i<NB_OBJETS;i++)

    cout<<"["<<val[i]<<","<<pds[i]<<"]"<<endl;
}


int main() {

    //initialisation
    int capacite = CAPA;
    /* // init fixé (pour le debug)
    std::vector<int> poids = {2, 3, 5, 7, 1, 4, 1};
    std::vector<int> valeurs = {10, 5, 15, 7, 8, 9, 4};
    afficherbis(capacite,poids,valeurs); //on affiche les paramètres actuels
    */
 
    //init avec aléatoire
    std::vector<int> poids=genererVecteurAleatoire(NB_OBJETS, 0, 10);
    std::vector<int> valeurs=genererVecteurAleatoire(NB_OBJETS,0,20);
    afficher(capacite,poids,valeurs); //on affiche les paramètres actuels



    //execution de l'algorithme de résoliution exacte
    Resultat best_resultat;
    Resultat actual_resultat;

   
    //afin d'éviter un débordement mémoire on utilise la mémoïsation :
    std::vector<std::vector<int>> memo(NB_OBJETS, std::vector<int>(CAPA + 1, -1));
    std::vector<Objet> obj =creerListeObjets(poids,valeurs);
    auto start_time = std::chrono::high_resolution_clock::now();

    Resultat resultat = parcours_exacte(obj, capacite, memo, NB_OBJETS - 1);

    auto end_time = std::chrono::high_resolution_clock::now();





    //retour algorithme
    cout << "Objets à prendre : ";
    for (int indice : resultat.objets_pris) {
        std::cout << indice << " ";
    }
    cout << "\nPoids utilisé : " << resultat.poids_utilise ;
    if (resultat.poids_utilise==capacite) cout <<" --> Aucune perte de place"<<endl;
    else cout<<" --> Perte de "<<capacite-resultat.poids_utilise<<" place(s)"<<endl;
    cout << "Valeur totale : " << resultat.valeur_totale << std::endl;
    

    // temps de calcul
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end_time - start_time);
    cout<< "\nUtilisation de l'algorithme glouton";
    std::cout << " (" << duration.count() << " ms)\n" << std::endl;
   


    return 0;
}
