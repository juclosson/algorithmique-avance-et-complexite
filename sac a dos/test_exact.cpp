#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <ctime>
#include <chrono>
#include <random>

using namespace std;

int CAPA=10;


//------------------------------Copie de resolution_exacte.cpp--------------------------------------

/*************************************************/
/****** On genère des vecteur aléatoirement ******/
std::vector<int> genererVecteurAleatoire(int taille, int valeurMin=0, int valeurMax=15) 
{
    // Initialiser le générateur de nombres aléatoires avec une graine basée sur l'horloge actuelle
    std::random_device rd;
    std::mt19937 generator(rd());

    // Définir la distribution pour les valeurs aléatoires entre valeurMin et valeurMax
    std::uniform_int_distribution<int> distribution(valeurMin, valeurMax);

    // Générer le vecteur avec des valeurs aléatoires
    std::vector<int> vecteur(taille);
    for (int& valeur : vecteur) 
    {
        valeur = distribution(generator);
    }
    return vecteur;
}



// une structure est crée pour chaque objet (cela facilitera le tri plus tard)
struct Objet {
    int poids;
    int valeur;
    int indice;

    Objet(int p, int v, int i) : poids(p), valeur(v), indice(i) {}
};

// on introduit un moyen de comparer 2 objets: selon leur efficacité
bool comparateur(const Objet& a, const Objet& b) {
    return (static_cast<double>(a.valeur) / a.poids) > (static_cast<double>(b.valeur) / b.poids);
}
//objet résultats, utile pour ne renvoyer qu'un objet avec les infos voulues
struct Resultat {
    std::vector<int> objets_pris;
    int poids_utilise;
    int valeur_totale;
};





//parcours exacte; on parcours tout l'arbre des solution et on ne garde que la meilleur 
Resultat parcours_exacte(std::vector<Objet>& objets, int capacite, std::vector<std::vector<int>>& memo, int index)
{
    if (index < 0) {
        return Resultat();  // Condition d'arrêt : plus d'objets à considérer
    }

    if (memo[index][capacite] != -1) {
        // La valeur a déjà été calculée pour ces paramètres
        return Resultat();
    }

    Resultat exclure = parcours_exacte(objets, capacite, memo, index - 1);

    if (objets[index].poids <= capacite) {
        Resultat inclure = parcours_exacte(objets, capacite - objets[index].poids, memo, index - 1);
        inclure.poids_utilise += objets[index].poids;
        inclure.valeur_totale += objets[index].valeur;
        inclure.objets_pris.push_back(objets[index].indice);

        if (inclure.valeur_totale > exclure.valeur_totale) {
            memo[index][capacite] = 1;
            return inclure;
        }
    }

    memo[index][capacite] = 1;
    return exclure;
}

// fonction qui permet de créer un liste d"Objet" aprtir des poids et valeurs, utile dans le cas de l'algo récurent
std::vector<Objet> creerListeObjets(const std::vector<int>& poids, const std::vector<int>& valeurs) {
    std::vector<Objet> objets;
    
    // Assurez-vous que les deux vecteurs ont la même taille
    if (poids.size() != valeurs.size()) {
        std::cerr << "Erreur : Les vecteurs poids et valeurs n'ont pas la même taille." << std::endl;
        // Gestion de l'erreur, retour ou traitement approprié
        // Dans cet exemple, je retourne simplement une liste vide
        return objets;
    }

    int n = poids.size();

    for (int i = 0; i < n; ++i) {
        objets.push_back(Objet(poids[i], valeurs[i], i));
    }

    return objets;
}


//fonction d'affichage des variables
void afficher(int capa, vector<int> pds , vector<int>& val) {
    
    cout<<"capacite disponible: "<<capa<<"\n";

    cout<<"Les objets disponibles ";
    cout<<"( [val,poids]) :\n";

    for(int i=0;i<pds.size();i++)

    cout<<"["<<val[i]<<","<<pds[i]<<"]"<<endl;
}


//-------------------------------Fonction main de tests------------------------------------

void test(bool assertion, int& nb_false, int& numero)
{
    if (!assertion)
    {
        nb_false ++;

        cout << "-------------------------------------------------" << endl;
        cout << "/!\\" << " Erreur au test numéro " << numero << " /!\\" << std::endl;
        cout << "-------------------------------------------------" << endl;
    }

    numero++;
}

int main() {

    //initialisation
    int capacite = CAPA;

    int nb_false = 0;
    int numero = 0;
    int NB_OBJETS;

    //-----------------test 0 : tableaux vides-----------------

    cout << std::endl << "-----------Tests à partir du numéro " << numero << "------------" << std::endl;

    std::vector<int> poids = {};
    std::vector<int> valeurs = {};
    NB_OBJETS = 0;

    afficher(capacite,poids,valeurs);

    std::vector<std::vector<int>> memo(NB_OBJETS, std::vector<int>(CAPA + 1, -1));
    std::vector<Objet> obj =creerListeObjets(poids,valeurs);
    
    Resultat resultat = parcours_exacte(obj, capacite, memo, NB_OBJETS - 1);

    test(resultat.poids_utilise == 0, nb_false, numero);
    test(resultat.valeur_totale == 0, nb_false, numero);
    test(resultat.objets_pris.size() == 0, nb_false, numero);

    //retour algorithme
    cout << "Objets à prendre : ";
    for (int indice : resultat.objets_pris) {
        std::cout << indice << " ";
    }
    cout << "\nPoids utilisé : " << resultat.poids_utilise ;
    if (resultat.poids_utilise==capacite) cout <<" --> Aucune perte de place"<<endl;
    else cout<<" --> Perte de "<<capacite-resultat.poids_utilise<<" place(s)"<<endl;
    cout << "Valeur totale : " << resultat.valeur_totale << std::endl;

    //-----------------test 1 : aucun objet ne passe-----------------

    cout << std::endl << "-----------Tests à partir du numéro " << numero << "------------" << std::endl;

    std::vector<int> poids1 = {11, 20};
    std::vector<int> valeurs1 = {2, 18};
    NB_OBJETS = 2;

    afficher(capacite,poids1,valeurs1);

    std::vector<std::vector<int>> memo1(NB_OBJETS, std::vector<int>(CAPA + 1, -1));
    std::vector<Objet> obj1 =creerListeObjets(poids1,valeurs1);
    
    resultat = parcours_exacte(obj1, capacite, memo1, NB_OBJETS - 1);

    test(resultat.poids_utilise == 0, nb_false, numero);
    test(resultat.valeur_totale == 0, nb_false, numero);
    test(resultat.objets_pris.size() == 0, nb_false, numero);

    //retour algorithme
    cout << "Objets à prendre : ";
    for (int indice : resultat.objets_pris) {
        std::cout << indice << " ";
    }
    cout << "\nPoids utilisé : " << resultat.poids_utilise ;
    if (resultat.poids_utilise==capacite) cout <<" --> Aucune perte de place"<<endl;
    else cout<<" --> Perte de "<<capacite-resultat.poids_utilise<<" place(s)"<<endl;
    cout << "Valeur totale : " << resultat.valeur_totale << std::endl;

    //-----------------test 2 : les objets passent tout pile-----------------

    cout << std::endl << "-----------Tests à partir du numéro " << numero << "------------" << std::endl;

    std::vector<int> poids2 = {6, 4};
    std::vector<int> valeurs2 = {2, 18};
    NB_OBJETS = 2;

    afficher(capacite,poids2,valeurs2);

    std::vector<std::vector<int>> memo2(NB_OBJETS, std::vector<int>(CAPA + 1, -1));
    std::vector<Objet> obj2 =creerListeObjets(poids2, valeurs2);
    
    resultat = parcours_exacte(obj2, capacite, memo2, NB_OBJETS - 1);

    test(resultat.poids_utilise == 10, nb_false, numero);
    test(resultat.valeur_totale == 20, nb_false, numero);
    test(resultat.objets_pris.size() == 2, nb_false, numero);

    //retour algorithme
    cout << "Objets à prendre : ";
    for (int indice : resultat.objets_pris) {
        std::cout << indice << " ";
    }
    cout << "\nPoids utilisé : " << resultat.poids_utilise ;
    if (resultat.poids_utilise==capacite) cout <<" --> Aucune perte de place"<<endl;
    else cout<<" --> Perte de "<<capacite-resultat.poids_utilise<<" place(s)"<<endl;
    cout << "Valeur totale : " << resultat.valeur_totale << std::endl;

    //-----------------test 3 : Cas optimal-----------------

    cout << std::endl << "-----------Tests à partir du numéro " << numero << "------------" << std::endl;

    std::vector<int> poids3 = {6, 5};
    std::vector<int> valeurs3 = {2, 18};
    NB_OBJETS = 2;

    afficher(capacite,poids3,valeurs3);

    std::vector<std::vector<int>> memo3(NB_OBJETS, std::vector<int>(CAPA + 1, -1));
    std::vector<Objet> obj3 =creerListeObjets(poids3, valeurs3);
    
    resultat = parcours_exacte(obj3, capacite, memo3, NB_OBJETS - 1);

    test(resultat.poids_utilise == 5, nb_false, numero);
    test(resultat.valeur_totale == 18, nb_false, numero);
    test(resultat.objets_pris.size() == 1, nb_false, numero);
    test(resultat.objets_pris[0] == 1, nb_false, numero);

    //retour algorithme
    cout << "Objets à prendre : ";
    for (int indice : resultat.objets_pris) {
        std::cout << indice << " ";
    }
    cout << "\nPoids utilisé : " << resultat.poids_utilise ;
    if (resultat.poids_utilise==capacite) cout <<" --> Aucune perte de place"<<endl;
    else cout<<" --> Perte de "<<capacite-resultat.poids_utilise<<" place(s)"<<endl;
    cout << "Valeur totale : " << resultat.valeur_totale << std::endl;

    //-----------------test 4 : Cas non optimal pour le glouton-----------------

    cout << std::endl << "-----------Tests à partir du numéro " << numero << "------------" << std::endl;

    std::vector<int> poids4 = {6, 5, 5};
    std::vector<int> valeurs4 = {6, 4, 4};  // Le 1er a une efficacité de 1 tandis que les suivants <1
    NB_OBJETS = 3;

    afficher(capacite,poids4,valeurs4);

    std::vector<std::vector<int>> memo4(NB_OBJETS, std::vector<int>(CAPA + 1, -1));
    std::vector<Objet> obj4 =creerListeObjets(poids4, valeurs4);
    
    resultat = parcours_exacte(obj4, capacite, memo4, NB_OBJETS - 1);

    test(resultat.poids_utilise == 10, nb_false, numero);
    test(resultat.valeur_totale == 8, nb_false, numero);
    test(resultat.objets_pris.size() == 2, nb_false, numero);

    //retour algorithme
    cout << "Objets à prendre : ";
    for (int indice : resultat.objets_pris) {
        std::cout << indice << " ";
    }
    cout << "\nPoids utilisé : " << resultat.poids_utilise ;
    if (resultat.poids_utilise==capacite) cout <<" --> Aucune perte de place"<<endl;
    else cout<<" --> Perte de "<<capacite-resultat.poids_utilise<<" place(s)"<<endl;
    cout << "Valeur totale : " << resultat.valeur_totale << std::endl;

    //-----------------Test faux pour vérifier le fonctionnement-----------------

    // test(false, nb_false, numero);

    //-----------------Fin des tests, affichage final-----------------

    cout << std::endl;
    cout << "Nombre d'erreurs : " << nb_false << "/" << numero << endl << endl;

    return 0;
}