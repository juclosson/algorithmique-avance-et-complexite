#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <ctime>
#include <chrono>
#include <random>

using namespace std;

//------------------------------Copie de resolution_glouton.cpp--------------------------------------

int NB_OBJETS=7;
int CAPA=10;


/*************************************************/
/****** On genère des vecteur aléatoirement ******/
std::vector<int> genererVecteurAleatoire(int taille, int valeurMin=0, int valeurMax=15) 
{
    // Initialiser le générateur de nombres aléatoires avec une graine basée sur l'horloge actuelle
    std::random_device rd;
    std::mt19937 generator(rd());

    // Définir la distribution pour les valeurs aléatoires entre valeurMin et valeurMax
    std::uniform_int_distribution<int> distribution(valeurMin, valeurMax);

    // Générer le vecteur avec des valeurs aléatoires
    std::vector<int> vecteur(taille);
    for (int& valeur : vecteur) 
    {
        valeur = distribution(generator);
    }
    return vecteur;
}



// une structure est crée pour chaque objet (cela facilitera le tri plus tard)
struct Objet {
    int poids;
    int valeur;
    int indice;

    Objet(int p, int v, int i) : poids(p), valeur(v), indice(i) {}
};

// on introduit un moyen de comparer 2 objets: selon leur efficacité
bool comparateur(const Objet& a, const Objet& b) {
    return (static_cast<double>(a.valeur) / a.poids) > (static_cast<double>(b.valeur) / b.poids);
}
//objet résultats, utile pour ne renvoyer qu'un objet avec les infos voulues
struct Resultat {
    std::vector<int> objets_pris;
    int poids_utilise;
    int valeur_totale;
};





//algorithme principale
Resultat knapsack_greedy(std::vector<int>& poids, std::vector<int>& valeurs, int capacite) {
    int n = poids.size();
    std::vector<Objet> objets;

    // Créer un vecteur d'objets avec poids, valeurs et indices associés
    for (int i = 0; i < n; ++i) {
        objets.push_back(Objet(poids[i], valeurs[i], i));
    }

    // Trier les objets par rapport valeur/poids dans l'ordre décroissant
    std::sort(objets.begin(), objets.end(), comparateur);

    Resultat resultat;
    resultat.poids_utilise = 0;
    resultat.valeur_totale = 0;

    // Sélectionner les objets tant que la capacité n'est pas dépassée
    for (const Objet& objet : objets) {
        if (resultat.poids_utilise + objet.poids <= capacite) {
            resultat.objets_pris.push_back(objet.indice);
            resultat.poids_utilise += objet.poids;
            resultat.valeur_totale += objet.valeur;
        }
    }

    return resultat;
}





//fonction d'affichage des variables

void afficher(int capa, vector<int> pds , vector<int>& val) {
    
    cout<<"capacite disponible: "<<capa<<"\n";

    cout<<"Les objets disponibles ";
    cout<<"( [val,poids]) :\n";

    for(int i=0;i<pds.size();i++)   //NB_OBJET remplacé par pds.size() pour ajuster aux tests

    cout<<"["<<val[i]<<","<<pds[i]<<"]"<<endl;
}


//-------------------------------Fonction main de tests------------------------------------

void test(bool assertion, int& nb_false, int& numero)
{
    if (!assertion)
    {
        nb_false ++;

        cout << "-------------------------------------------------" << endl;
        cout << "/!\\" << " Erreur au test numéro " << numero << " /!\\" << std::endl;
        cout << "-------------------------------------------------" << endl;
    }

    numero++;
}

int main() {

    //initialisation
    int capacite = CAPA;

    int nb_false = 0;
    int numero = 0;

    //-----------------test 0 : tableaux vides-----------------

    cout << std::endl << "-----------Tests à partir du numéro " << numero << "------------" << std::endl;

    std::vector<int> poids = {};
    std::vector<int> valeurs = {};
    afficher(capacite,poids,valeurs);

    Resultat resultat = knapsack_greedy(poids, valeurs, capacite);

    test(resultat.poids_utilise == 0, nb_false, numero);
    test(resultat.valeur_totale == 0, nb_false, numero);
    test(resultat.objets_pris.size() == 0, nb_false, numero);

    //retour algorithme
    cout << "Objets à prendre : ";
    for (int indice : resultat.objets_pris) {
        std::cout << indice << " ";
    }
    cout << "\nPoids utilisé : " << resultat.poids_utilise ;
    if (resultat.poids_utilise==capacite) cout <<" --> Aucune perte de place"<<endl;
    else cout<<" --> Perte de "<<capacite-resultat.poids_utilise<<" place(s)"<<endl;
    cout << "Valeur totale : " << resultat.valeur_totale << std::endl;

    //-----------------test 1 : aucun objet ne passe-----------------

    cout << std::endl << "-----------Tests à partir du numéro " << numero << "------------" << std::endl;

    std::vector<int> poids1 = {11, 20};
    std::vector<int> valeurs1 = {2, 18};
    afficher(capacite,poids1,valeurs1);

    resultat = knapsack_greedy(poids1, valeurs1, capacite);

    test(resultat.poids_utilise == 0, nb_false, numero);
    test(resultat.valeur_totale == 0, nb_false, numero);
    test(resultat.objets_pris.size() == 0, nb_false, numero);

    //retour algorithme
    cout << "Objets à prendre : ";
    for (int indice : resultat.objets_pris) {
        std::cout << indice << " ";
    }
    cout << "\nPoids utilisé : " << resultat.poids_utilise ;
    if (resultat.poids_utilise==capacite) cout <<" --> Aucune perte de place"<<endl;
    else cout<<" --> Perte de "<<capacite-resultat.poids_utilise<<" place(s)"<<endl;
    cout << "Valeur totale : " << resultat.valeur_totale << std::endl;

    //-----------------test 2 : les objets passent tout pile-----------------

    cout << std::endl << "-----------Tests à partir du numéro " << numero << "------------" << std::endl;

    std::vector<int> poids2 = {6, 4};
    std::vector<int> valeurs2 = {2, 18};
    afficher(capacite,poids2,valeurs2);

    resultat = knapsack_greedy(poids2, valeurs2, capacite);

    test(resultat.poids_utilise == 10, nb_false, numero);
    test(resultat.valeur_totale == 20, nb_false, numero);
    test(resultat.objets_pris.size() == 2, nb_false, numero);

    //retour algorithme
    cout << "Objets à prendre : ";
    for (int indice : resultat.objets_pris) {
        std::cout << indice << " ";
    }
    cout << "\nPoids utilisé : " << resultat.poids_utilise ;
    if (resultat.poids_utilise==capacite) cout <<" --> Aucune perte de place"<<endl;
    else cout<<" --> Perte de "<<capacite-resultat.poids_utilise<<" place(s)"<<endl;
    cout << "Valeur totale : " << resultat.valeur_totale << std::endl;

    //-----------------test 3 : Cas optimal-----------------

    cout << std::endl << "-----------Tests à partir du numéro " << numero << "------------" << std::endl;

    std::vector<int> poids3 = {6, 5};
    std::vector<int> valeurs3 = {2, 18};
    afficher(capacite,poids3,valeurs3);

    resultat = knapsack_greedy(poids3, valeurs3, capacite);

    test(resultat.poids_utilise == 5, nb_false, numero);
    test(resultat.valeur_totale == 18, nb_false, numero);
    test(resultat.objets_pris.size() == 1, nb_false, numero);
    test(resultat.objets_pris[0] == 1, nb_false, numero);

    //retour algorithme
    cout << "Objets à prendre : ";
    for (int indice : resultat.objets_pris) {
        std::cout << indice << " ";
    }
    cout << "\nPoids utilisé : " << resultat.poids_utilise ;
    if (resultat.poids_utilise==capacite) cout <<" --> Aucune perte de place"<<endl;
    else cout<<" --> Perte de "<<capacite-resultat.poids_utilise<<" place(s)"<<endl;
    cout << "Valeur totale : " << resultat.valeur_totale << std::endl;

    //-----------------test 4 : Cas non optimal pour le glouton-----------------

    cout << std::endl << "-----------Tests à partir du numéro " << numero << "------------" << std::endl;

    std::vector<int> poids4 = {6, 5, 5};
    std::vector<int> valeurs4 = {6, 4, 4};  // Le 1er a une efficacité de 1 tandis que les suivants <1
    afficher(capacite,poids4,valeurs4);

    resultat = knapsack_greedy(poids4, valeurs4, capacite);

    test(resultat.poids_utilise == 6, nb_false, numero);
    test(resultat.valeur_totale == 6, nb_false, numero);
    test(resultat.objets_pris.size() == 1, nb_false, numero);
    test(resultat.objets_pris[0] == 0, nb_false, numero);

    //retour algorithme
    cout << "Objets à prendre : ";
    for (int indice : resultat.objets_pris) {
        std::cout << indice << " ";
    }
    cout << "\nPoids utilisé : " << resultat.poids_utilise ;
    if (resultat.poids_utilise==capacite) cout <<" --> Aucune perte de place"<<endl;
    else cout<<" --> Perte de "<<capacite-resultat.poids_utilise<<" place(s)"<<endl;
    cout << "Valeur totale : " << resultat.valeur_totale << std::endl;

    //-----------------Test faux pour vérifier le fonctionnement-----------------

    //test(false, nb_false, numero);

    //-----------------Fin des tests, affichage final-----------------

    cout << std::endl;
    cout << "Nombre d'erreurs : " << nb_false << "/" << numero << endl << endl;

    return 0;
}