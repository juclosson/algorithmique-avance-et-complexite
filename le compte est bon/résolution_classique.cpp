#include <iostream>
#include <vector>
#include <random>
#include <algorithm>
#include <chrono>
using namespace std;




// tout les nombres que l'on peut tirer
std:: vector<int> nb_dispo ={1,2,3,4,5,6,7,8,9,10,25,50,100};


/*****************************************************/
/* fonction: Initialisation                          */
/* retourne un vecteur de 6 entiers parmis           */
/* ceux situés dans nb_dispo ci dessus               */
/* entrée: aucune                                    */
/* sortie: vecteur de 6 int                          */
/*****************************************************/
std::vector<int> initialisation()
{


    // Utiliser un générateur pseudo-aléatoire pour mélanger la liste
    std::random_device rd;
    std::mt19937 mt(rd());
    std::shuffle(nb_dispo.begin(), nb_dispo.end(), mt);

    // Tirer les 6 premiers éléments mélangés
    std::vector<int> tirage(nb_dispo.begin(), nb_dispo.begin() + 6);

    return tirage;
}




/**********************************************************************************/
/*          fonction: compteEstBon                                                */
/*                                                                                */
/*  prend les deux premières valeurs et on tente de rapeler l'algorithme  avec    */
/*  toutes les opérations entre ces deux valeurs, on procède de manière récursive */
/*  jusqu'à etre à une seul valeur                                                */
/*  et retourne un booléen qui indique si le compte est bon                       */
/*                                                                                */
/* entré: un vecteur de int, un int et un vecteur de int par référence            */
/* sortie: un bouléen                                                             */
/**********************************************************************************/
bool compteEstBon_naif(vector<int>& nombres, int objectif, vector<int>& solution) {
    if (nombres.size() == 1) {
        // Si la liste ne contient qu'un seul nombre, on vérifie s'il est égal à l'objectif
        if (nombres[0] == objectif) {
            return true;
        } else {
            return false;
        }
    }

    // Essayer toutes les paires de nombres et toutes les opérations
    for (size_t i = 0; i < nombres.size(); ++i) {
        for (size_t j = i + 1; j < nombres.size(); ++j) {
            int a = max(nombres[i], nombres[j]);
            int b = min(nombres[i], nombres[j]);

            // Effectuer les opérations
            vector<int> reste;
            reste.reserve(nombres.size() - 2);
            for (size_t k = 0; k < nombres.size(); ++k) {
                if (k != i && k != j) {
                    reste.push_back(nombres[k]);
                }
            }

            // Addition
            reste.push_back(a + b);
            solution.push_back(a);
            solution.push_back('+');
            solution.push_back(b);
            solution.push_back('=');
            solution.push_back(a+b);

            if (compteEstBon_naif(reste, objectif, solution)) {
                return true;
            }
            solution.pop_back();
            solution.pop_back();
            solution.pop_back();
            solution.pop_back();
            solution.pop_back();

             //(vérifier que a!=b)
            if (a!=b) {
                reste.back() = a - b;
                solution.push_back(a);
                solution.push_back('-');
                solution.push_back(b);
                solution.push_back('=');
                solution.push_back(a-b);


                if (compteEstBon_naif(reste, objectif, solution)) {
                    return true;
                }
                solution.pop_back();
                solution.pop_back();
                solution.pop_back();
                solution.pop_back();
                solution.pop_back();
            }

            // Multiplication
            reste.back() = a * b;
            solution.push_back(a);
            solution.push_back('*');
            solution.push_back(b);
            solution.push_back('=');
            solution.push_back(a*b);

            if (compteEstBon_naif(reste, objectif, solution)) {
                return true;
            }
            solution.pop_back();
            solution.pop_back();
            solution.pop_back();
            solution.pop_back();
            solution.pop_back();

            // Division (vérifier que le résultat est entier et positif)( b/a<0 donc seul a/b est calculé)
            if (b != 0 && a % b == 0) {
                reste.back() = a / b;
                solution.push_back(a);
                solution.push_back('/');
                solution.push_back(b);
                solution.push_back('=');
                solution.push_back(a/b);

                if (compteEstBon_naif(reste, objectif, solution)) {
                    return true;
                }
                solution.pop_back();
                solution.pop_back();
                solution.pop_back();
                solution.pop_back();
                solution.pop_back();
            }
        }
    }

    return false;
}




int main() {
    

    int objectif = 100;
    vector<int> solution;
    int ligne=0; // variable pour l'affichage


    /**** initialisation ****/
    // Tirage des valeurs disponibles
    std::vector<int> nombres = initialisation();
    std::cout << "les nombres tirés sont: ";
    for (int num : nombres) 
    {
        std::cout << num << " ";
    }
    std::cout << std::endl;
    

    //Tirage de l'objectif
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<int> distribution(100, 999);
    objectif= distribution(mt);
    std::cout <<"l'objectif est "<<objectif<<"\n";


    /*
    //On peut chosir les valeurs afin de débugger.
    objectif=100;
    nombres={1,2,3,4,5,6}
    */



    //start timmer
    auto start_time = std::chrono::high_resolution_clock::now();


    if (compteEstBon_naif(nombres, objectif, solution)) {
        cout << "Solution trouvee : \n";

        // affichage de la solution et du chemin pour y arriver
        for (size_t i = 0; i < solution.size()-2; ++i) {
            if (solution[i] == '+' || solution[i] == '-' || solution[i] == '*'
                || solution[i] == '/') {
                cout << " " << char(solution[i]) << " ";
            }
            else if (solution[i] == '=')   
            {
                cout<<" "<< char(solution[i])<<" ";
                ligne=1;
            }
            else {
                cout << solution[i] << " ";
                if (ligne==1) 
                {
                    cout<< "\n"; 
                    ligne=0;
                }  
            }
        }
        cout << "= " << objectif << endl;
    } 
    else {

        //cas rare mais possible du cas où pas de solution
        cout << "Aucune solution trouvee." << endl;
    }








    /***** partie timer  ******/
    // Enregistrez le temps de fin
    auto end_time = std::chrono::high_resolution_clock::now();
    // Calcule de la durée écoulée
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end_time - start_time);
    std::cout << "Temps d'execution : " << duration.count() << " microsecondes" << std::endl;

    return 0;
}
