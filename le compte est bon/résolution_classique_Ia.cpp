#include <iostream>
#include <vector>
#include <chrono>
#include <random>
#include <algorithm>
using namespace std;



// tout les nombres que l'on peut tirer
std:: vector<int> nb_dispo ={1,2,3,4,5,6,7,8,9,10,25,50,100};


/*****************************************************/
/* fonction: Initialisation                          */
/* retourne un vecteur de 6 entiers parmis           */
/* ceux situés dans nb_dispo ci dessus               */
/* entrée: aucune                                    */
/* sortie: vecteur de 6 int                          */
/*****************************************************/
std::vector<int> initialisation()
{


    // Utiliser un générateur pseudo-aléatoire pour mélanger la liste
    std::random_device rd;
    std::mt19937 mt(rd());
    std::shuffle(nb_dispo.begin(), nb_dispo.end(), mt);

    // Tirer les 6 premiers éléments mélangés
    std::vector<int> tirage(nb_dispo.begin(), nb_dispo.begin() + 6);

    return tirage;
}



//voici l'algo 1 en synthétisé: on test tout on stocke la solution dans un vecteur de string: solution, incrémenté et réduit au fur et à mesure

bool compteEstBon_non_naif(vector<int>& nombres, int objectif, vector<string>& solution) {
    if (nombres.size() == 1) {
        if (nombres[0] == objectif) {
            return true;
        } else {
            return false;
        }
    }

    for (size_t i = 0; i < nombres.size(); ++i) {
        for (size_t j = i + 1; j < nombres.size(); ++j) {
            int a = max(nombres[i], nombres[j]);
            int b = min(nombres[i], nombres[j]);

            vector<int> reste;
            reste.reserve(nombres.size() - 2);
            for (size_t k = 0; k < nombres.size(); ++k) {
                if (k != i && k != j) {
                    reste.push_back(nombres[k]);
                }
            }

            // Addition
            reste.push_back(a + b);
            solution.push_back(to_string(a) + "+" + to_string(b) + "=" + to_string(a + b));

            if (compteEstBon_non_naif(reste, objectif, solution)) {
                return true;
            }
            solution.pop_back();

            // Soustraction (vérifier que a!=b, o est inutile pour les calculs)
            if (a != b) {
                reste.back() = a - b;
                solution.push_back(to_string(a) + "-" + to_string(b) + "=" + to_string(a - b));

                if (compteEstBon_non_naif(reste, objectif, solution)) {
                    return true;
                }
                solution.pop_back();
            }

            // Multiplication
            reste.back() = a * b;
            solution.push_back(to_string(a) + "*" + to_string(b) + "=" + to_string(a * b));

            if (compteEstBon_non_naif(reste, objectif, solution)) {
                return true;
            }
            solution.pop_back();

            // Division (vérifier que le résultat est entier)
            if (b != 0 && a % b == 0) {
                reste.back() = a / b;
                solution.push_back(to_string(a) + "/" + to_string(b) + "=" + to_string(a / b));

                if (compteEstBon_non_naif(reste, objectif, solution)) {
                    return true;
                }
                solution.pop_back();
            }
        }
    }

    return false;
}

int main() {

    //choix des paramètres
    vector<int> nombres = initialisation();
    int objectif = 765;
    vector<string> solution;



    //Tirage de l'objectif
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<int> distribution(100, 999);
    objectif= distribution(mt);
    std::cout <<"l'objectif est "<<objectif<<"\n";



    std::cout << "les nombres tirés sont: ";
    for (int num : nombres) 
    {
        std::cout << num << " ";
    }
    std::cout << std::endl;
    std::cout <<"l'objectif est "<<objectif<<"\n\n";
    auto start_time = std::chrono::high_resolution_clock::now();



    /*
    //On peut chosir les valeurs afin de débugger.
    objectif=100;
    nombres={1,2,3,4,5,6}
    */



    if (compteEstBon_non_naif(nombres, objectif, solution)) {
        cout << "Solution trouvée :" << endl;
        for (const string& step : solution) {
            cout << step << endl;
        }
    } else {
        cout << "Aucune solution trouvée." << endl;
    }



    /***** partie timer  ******/
    // Enregistrez le temps de fin
    auto end_time = std::chrono::high_resolution_clock::now();
    // Calcule de la durée écoulée
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end_time - start_time);
    std::cout << "Temps d'execution : " << duration.count() << " microsecondes" << std::endl;

    return 0;
}
