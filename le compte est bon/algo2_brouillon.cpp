#include <iostream>
#include <vector>
using namespace std;



/*
Cet algorithme n'est pas fonctionel, il repose sur le fait qu'un nombtre limité de combinaison
entre les plaques= chiffres tirée est possible, et tente toutes 
ces combinaisons plutoto que de tenter  tout de manière récursive

faute de temps et complexité, la mise en place n'a pas été effectué 

*/


// Déclarations anticipées des fonctions
void troisPlaques(const vector<int>&, const vector<int>&, int&);
void quatrePlaques(const vector<int>&, const vector<int>&, const vector<int>&, int&);
void cinqPlaques(const vector<int>&, const vector<int>&, int&);
void sixPlaques(const vector<int>&, const vector<int>&, const vector<int>&, int&);

// Fonction pour le cas de 2 plaques
void deuxPlaques(const vector<int>& plaques, int& count);

// Fonction pour le cas de 3 plaques
void troisPlaques(const vector<int>& partial, const vector<int>& remaining, int& count) {
    if (remaining.size() == 1) {
        count++;
    } else {
        for (size_t i = 0; i < remaining.size(); ++i) {
            // Opération R1 * pk = R2 (RP)
            int result = partial.back() * remaining[i];
            // Appel récursif pour le cas restant
            quatrePlaques(partial, {result}, remaining, count);
        }
    }
}

// Fonction pour le cas de 4 plaques
void quatrePlaques(const vector<int>& partial1, const vector<int>& partial2, const vector<int>& remaining, int& count) {
    for (size_t i = 0; i < partial1.size(); ++i) {
        for (size_t j = 0; j < partial2.size(); ++j) {
            // Opération R1 * R2 = R3 (RR)
            int result = partial1[i] * partial2[j];
            // Appel récursif pour le cas restant
            cinqPlaques({result}, remaining, count);
        }
    }
}

// Fonction pour le cas de 5 plaques
void cinqPlaques(const vector<int>& partial, const vector<int>& remaining, int& count) {
    for (size_t i = 0; i < partial.size(); ++i) {
        for (size_t j = 0; j < remaining.size(); ++j) {
            // Opération R1 * pk = R2 (RP)
            int result = partial[i] * remaining[j];
            // Appel récursif pour le cas restant
            sixPlaques({result}, {}, remaining, count);

        }
    }
}


// Fonction pour le cas de 6 plaques
// Fonction pour le cas de 6 plaques
void sixPlaques(const vector<int>& partial1, const vector<int>& partial2, const vector<int>& remaining, int& count) {
    for (size_t i = 0; i < partial1.size(); ++i) {
        for (size_t j = 0; j < partial2.size(); ++j) {
            // Opération R1 * R2 = R3 (RR)
            int result = partial1[i] * partial2[j];
            // Appel récursif pour le cas restant
            cinqPlaques({result}, remaining, count);
        }
    }
}


// Fonction pour le cas de 2 plaques
void deuxPlaques(const vector<int>& plaques, int& count) {
    for (size_t i = 0; i < plaques.size(); ++i) {
        for (size_t j = i + 1; j < plaques.size(); ++j) {
            // Opération pi * pj = R1 (PP)
            int result = plaques[i] * plaques[j];
            // Appel récursif pour le cas restant
            troisPlaques({result}, plaques, count);
        }
    }
}

int main() {
    vector<int> plaques = {1, 2, 3, 4, 5, 6};
    int count = 0;

    // Appel de la fonction pour le cas de 2 plaques
    deuxPlaques(plaques, count);

    // Affichage du nombre total d'opérations
    cout << "Nombre total d'operations : " << count << endl;

    return 0;
}
